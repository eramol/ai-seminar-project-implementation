package tests;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import org.junit.Test;

import structures.C;
import structures.Event;
import structures.Segment;

/**
 * I have this test just for testing the code behaviour
 * @author Martin
 *
 */
public class DummyTest {

	@Test
	public void test() {
		Event e = new Event(new Point2D.Double(2,3));
		PriorityQueue<Event> pq = new PriorityQueue<>();
		pq.add(e);
		
		e.getC().add(new Segment(null, 0));
		assertEquals(1, pq.peek().getC().size());
	}
	
	@Test
	public void test2() {
		List<Integer> lst = new ArrayList<>();
		int a = 1;
		for(Integer i : lst) {
			a = i;
		}
		assertEquals(1, a);
		
	}
	
	@Test
	public void test3() {
		List<Integer> a = new ArrayList<>();
		a.add(0,1);
		a.add(2);
		a.add(4);
		a.add(2,3);
		
		
		int x = a.get(2);
		assertEquals(3, x);
		
		a.remove(0);
		x = a.get(2);
		assertEquals(4, x);
	}
	
	
	@Test
	public void test4() {
		Point2D a = new Point2D.Double(64.00000000000001, 33.00000000000006);
		Point2D b = new Point2D.Double(64.0, 33.0);
		
		assertTrue(C.comparePoints(a, b) == 0);
		
		
		
	}

}
