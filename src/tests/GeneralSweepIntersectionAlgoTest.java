package tests;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import algo.GeneralSweepIntersectionAlgo;
import algo.NaiveIntersectionAlgo;
import structures.Event;
import structures.Segment;
import structures.imple.ListSegmentStructure;
import structures.imple.MapPQEventQueue;
import utils.Utility;

/**
 * very important test suite, testing correctness and runability of the sweep algo
 * some naive testing algo added for comparison
 * 
 * @author Martin
 *
 */
public class GeneralSweepIntersectionAlgoTest {

	@Test
	public void staticDetectIntersectionFromEventTest() {				
		Event e = new Event(null);
		e.getC().add(new Segment(null, 0));
		
		assertEquals(false, GeneralSweepIntersectionAlgo.checkForIntersection(e));
		
		e.getC().add(new Segment(null, 0));
		e.getL().add(new Segment(null, 0));
		e.getU().add(new Segment(null, 0));
		
		
		assertEquals(false, GeneralSweepIntersectionAlgo.checkForIntersection(e));
		
		e.getC().add(new Segment(null, 1));
		e.getL().add(new Segment(null, 0));
		e.getU().add(new Segment(null, 0));
		
		assertEquals(true, GeneralSweepIntersectionAlgo.checkForIntersection(e));						
	}
	
	
	@Test
	public void basicDetectIntersectionsTest() {		
		GeneralSweepIntersectionAlgo gsi = new GeneralSweepIntersectionAlgo(new MapPQEventQueue(),
																			new ListSegmentStructure());
		
		gsi.setup("test/basicCase/A.txt", "test/basicCase/B.txt");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
		assertEquals(1, lst.size());
		assertEquals(0.5, lst.get(0).getX(), 0.0001);
		assertEquals(0.5, lst.get(0).getY(), 0.0001);					
	}
	
	@Test
	public void advancedDetectIntersectionTest() {		
		GeneralSweepIntersectionAlgo gsi = new GeneralSweepIntersectionAlgo(new MapPQEventQueue(),
				new ListSegmentStructure());

		gsi.setup("test/advancedCase/A.txt", "test/advancedCase/B.txt");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
		assertEquals(5, lst.size());
		
		
	}
	
	@Test
	public void randomTest() {
		GeneralSweepIntersectionAlgo gsi = new GeneralSweepIntersectionAlgo(new MapPQEventQueue(),
				new ListSegmentStructure());
		gsi.setup("test/tmp/tmpA", "test/tmp/tmpB");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
		Set<Event> hs = new HashSet<>();
		System.out.println(lst.size());
	}
	
	@Test
	public void randomTest2() throws FileNotFoundException, UnsupportedEncodingException {
		GeneralSweepIntersectionAlgo gsi = new GeneralSweepIntersectionAlgo(new MapPQEventQueue(),
				new ListSegmentStructure());
		Utility.Generate(100, "test/tmp/tmpA2");
		Utility.Generate(100, "test/tmp/tmpB2");
		gsi.setup("test/tmp/tmpA2", "test/tmp/tmpB2");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
//		System.out.println(lst.size());

	}
	
	/**
	 * 
	 */
	@Test
	public void randomNaiveTest() {
		NaiveIntersectionAlgo gsi = new NaiveIntersectionAlgo();
		gsi.setup("test/tmp/tmpA", "test/tmp/tmpB");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
		
		Set<Event> hs = new HashSet<>();
		for(Point2D p : lst) {
			hs.add(new Event(p));
		}
		System.out.println(hs.size());
		
		//part for comparison
//		List<Event> ls = new ArrayList(hs);
//		Collections.sort(ls);
//		for(Event e : ls)
//			System.out.print(e.getPoint() + " ");
//		System.out.println();
		

	}
	
	@Test
	public void advancedNaiveTest() {
		NaiveIntersectionAlgo gsi = new NaiveIntersectionAlgo();
		gsi.setup("test/advancedCase/A.txt", "test/advancedCase/B.txt");
		gsi.run();
		List<Point2D> lst = gsi.getResults();
		assertEquals(5, (new HashSet<>(lst)).size());

	}
	
//	@Test
	public void generate() throws FileNotFoundException, UnsupportedEncodingException {
		Utility.Generate(100, "test/tmp/tmpA");
		Utility.Generate(100, "test/tmp/tmpB");
	}
	
	
	

}