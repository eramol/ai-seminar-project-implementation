package tests;

import static org.junit.Assert.*;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.List;

import org.junit.Test;

import structures.C;
import structures.StaticSweepReference;
import data.DataHandler;


/**
 * tests for segment ordering correctness
 * @author Martin
 *
 */
public class SegmentTest {

	@Test
	public void comparePointsTest() {
		assertEquals(0, C.comparePoints(new Point2D.Double(2,2), new Point2D.Double(2,2)));
		assertEquals(1, C.comparePoints(new Point2D.Double(3,2), new Point2D.Double(2,2)));
		assertEquals(1, C.comparePoints(new Point2D.Double(1,2), new Point2D.Double(2,3)));
		assertEquals(-1, C.comparePoints(new Point2D.Double(2,2), new Point2D.Double(1,-1)));		
	}

	@Test
	public void compareLineSubjectToStaticYTest() {		
		List<Line2D> lst = (new DataHandler("test/segmentCompare.txt")).getRawSegments();

		//two non-horizontal lines		
		StaticSweepReference.setRefPoint(new Point2D.Double(0, 2));
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(0), lst.get(1)));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(1), lst.get(0)));
		assertEquals(0, C.compareLineSubjectToStaticY(lst.get(0), lst.get(0)));
		
		//two non-horizontal lines after intersection
		StaticSweepReference.setRefPoint(new Point2D.Double(0, 0));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(0), lst.get(1)));
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(1), lst.get(0)));
				
		//intersecting horizontal line and non-horizontal line
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(2), lst.get(3)));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(3), lst.get(2)));
		
		//two overlapping horizontal lines
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(4), lst.get(2)));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(2), lst.get(4)));
		
		//non-intersecting horizontal line and non-horizontal line
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(4), lst.get(1)));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(1), lst.get(4)));
		
		//comparison at the intersection
		assertEquals(1, C.compareLineSubjectToStaticY(lst.get(6), lst.get(5)));
		assertEquals(-1, C.compareLineSubjectToStaticY(lst.get(5), lst.get(6)));
		
		assertEquals(0, C.compareLineSubjectToStaticY(lst.get(0), lst.get(7)));
	}
	
	
	@Test
	public void computeIntersectionTest() {
		List<Line2D> lst = (new DataHandler("test/segmentIntersection.txt")).getRawSegments();
		
		assertEquals(new Point2D.Double(0,0), C.computeIntersectionLine2D(lst.get(0), lst.get(1)));
		assertEquals(new Point2D.Double(1,0), C.computeIntersectionLine2D(lst.get(2), lst.get(3)));
		
	}
}
