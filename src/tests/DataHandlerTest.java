package tests;

import static org.junit.Assert.*;

import java.awt.geom.Line2D;
import java.util.List;

import org.junit.Test;

import data.DataHandler;

/**
 * tests correct behaviour of the data handling part of the program
 * @author Martin
 *
 */
public class DataHandlerTest {

	/**
	 * 
	 */
	@Test
	public void functionalityTest() {
		DataHandler dh = new DataHandler("test/dhdata.txt");
		List<Line2D> lst = dh.getRawSegments();
		
		assertEquals(5, lst.size());
		assertEquals(3.23, lst.get(0).getX1(), 0.001);
		assertEquals(2, lst.get(4).getY2(), 0.001);
		assertEquals(4, lst.get(1).getY1(), 0.001);		
	}

}
