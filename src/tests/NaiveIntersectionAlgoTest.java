package tests;

import static org.junit.Assert.*;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.List;

import org.junit.Test;

import algo.NaiveIntersectionAlgo;

public class NaiveIntersectionAlgoTest {
	
	// Test if the overlapping of two segments is detected
	@Test
	public void overlappingDetectionTest() {
		NaiveIntersectionAlgo ni = new NaiveIntersectionAlgo();
		Point2D res = ni.getIntersection(new Line2D.Double(2.0, 2.0, 4.0, 4.0),	new Line2D.Double(2.0, 2.0, 4.0, 4.0));

		assertEquals(new Point2D.Double(-1.0, -1.0), res);
	}
	
	// Test if the intersections are detected correctly
	@Test
	public void intersectionDetectionTest() {
		NaiveIntersectionAlgo ni = new NaiveIntersectionAlgo();
		ni.setup("test/basicCase/A.txt", "test/basicCase/B.txt");
		ni.run();
		List<Point2D> lst = ni.getResults();
		assertEquals(1, lst.size());
		assertEquals(0.5, lst.get(0).getX(), 0.0001);
		assertEquals(0.5, lst.get(0).getY(), 0.0001);
	}

}
