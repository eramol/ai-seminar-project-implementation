package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DataHandlerTest.class, DummyTest.class, EventQueueTest.class,
		NaiveIntersectionAlgoTest.class,GeneralSweepIntersectionAlgoTest.class, //ListSegmentStructureTest.class,
		SegmentTest.class })
public class AllTests {

}
