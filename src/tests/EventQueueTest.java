package tests;

import static org.junit.Assert.*;

import java.awt.geom.Point2D;

import org.junit.Test;

import structures.Event;
import structures.IEventQueue;
import structures.Segment;
import structures.imple.MapPQEventQueue;

/**
 * a big test of functionality of the EventQueue
 * @author Martin
 *
 */
public class EventQueueTest {
	
	@Test
	public void MapPQEventQueueTest() {
		eventQueueTestBody(new MapPQEventQueue());
	}
	
	public void eventQueueTestBody(IEventQueue eq) {		
		Event e;
		
		eq.add(new Event(new Point2D.Double(2,3)));
		eq.add(new Event(new Point2D.Double(1,1)));
		eq.add(new Event(new Point2D.Double(1,2)));
		eq.add(new Event(new Point2D.Double(2,1)));
		eq.add(new Event(new Point2D.Double(3,3)));
		
		e = eq.search(new Point2D.Double(5, 5));
		assertEquals(null, e);
		
		e = eq.search(new Point2D.Double(1, 2.2));
		assertEquals(null, e);
		
		e = eq.search(new Point2D.Double(3, 3));
		assertNotEquals(null, e);
		e.getC().add(new Segment(null, 0));
		
		
		e = eq.removeHead();
		assertEquals(new Point2D.Double(2,3), e.getPoint());
		
		e = eq.removeHead();
		assertEquals(new Point2D.Double(3,3), e.getPoint());
		assertEquals(1, e.getC().size());
		assertEquals(0, e.getC().get(0).label);	
	}
	

}
