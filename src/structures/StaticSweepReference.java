package structures;

import java.awt.geom.Point2D;

/**
 * this is a structure that handles the position of the sweepline
 * it has a memory, because the structure
 * @author Martin
 *
 */
public class StaticSweepReference {
	
	private static Point2D refPoint = null;
	private static Point2D refPointB = null;
	
	/**
	 * get reference point (the only important thing is the Y coordinate)
	 * @return a reference point
	 */
	public static Point2D getRefPoint() {
		//System.out.println("gRP:" + StaticSweepReference.refPoint);
		return refPoint;
	}

	/**
	 * set reference point, if the new point doesn't have different y axis, ignore
	 * @param refPoint
	 */
	public static void setRefPoint(Point2D refPoint) {
		if(StaticSweepReference.refPoint != null){						
			if(C.comparePoints(refPoint, StaticSweepReference.refPoint) != 0) {
				StaticSweepReference.refPointB = StaticSweepReference.refPoint;	
			}
			StaticSweepReference.refPoint = refPoint;
		} else {
			StaticSweepReference.refPoint = refPointB = refPoint;
		}
	}
	
	/**
	 * restores the previous reference point with different axis
	 */
	public static void restoreRefPoint() {		
		refPoint = refPointB;
	}

	
}
