package structures;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * a class that should statically encapsulate all the geometrical computations
 *  
 * @author Martin
 *
 */
public class C {
	public static final double ERROR_RATE = 0.0000001;
	public static final double ZERO_SUB = 0.000001;
	
	
	public static int comparePoints(Point2D a, Point2D b) {			
			if(C.dE(a.getY(), b.getY()))
				return C.dE(a.getX(), b.getX()) ? 0 : Double.compare(a.getX(), b.getX());
			else
				return C.dE(b.getY(), a.getY()) ? 0 : Double.compare(b.getY(), a.getY());
	}
	
	/**
	 * WARNING: not very clean method, dependent on the state of StaticSweepReference class
	 *  
	 * compares two lines subject to the static y coordinate
	 * the y coordinate is set using StaticSweepReference class
	 * 
	 * @param a first line
	 * @param b second line
	 * @param y reference y
	 * @return
	 */
	public static int compareLineSubjectToStaticY(Line2D a, Line2D b) {
		if(a == b)
			return 0;
		
		Double myx = estimateX(a, StaticSweepReference.getRefPoint().getY());
		Double ox = estimateX(b, StaticSweepReference.getRefPoint().getY());		
		
		if(myx == null && ox == null) {
			//both are horizontal => compare their starting points
			return C.comparePoints(a.getP1(), b.getP1());
		} else if(myx == null || ox == null) {
			//one is horizontal
			if(a.intersectsLine(b)) {				
				//if they intersect, the non-horizontal is higher (will have lower tangent)
				return (myx == null) ? 1 : -1;
			} else {
				//if they don't intersect, arbitrary point of the horizontal line is chosen
				return (myx == null) ? Double.compare(a.getX1(), ox) : Double.compare(myx, b.getX1());
			}
		} else {
			
			//System.out.println("Comparison ("+ a.getP1() + "," + b.getP1() + ") "  + myx + " " + ox);
			//neither of them is horizontal
						
			//if they don't intersect at the y
			if(!dE(myx, ox))
				return Double.compare(myx, ox);						
			
			
			//else return a comparison of their cotangents
			double myAngle = Math.atan2(a.getY2() - a.getY1(), a.getX2() - a.getX1());
			
			double oAngle = Math.atan2(b.getY2() - b.getY1(), b.getX2() - b.getX1());
			
			//System.out.println("MA:" + myAngle + " OA:" + oAngle);
			
			
			if(!dE(myAngle, oAngle)) {
				return Double.compare(myAngle, oAngle);
			} else {
				return C.comparePoints(a.getP2(), b.getP2());
			}
				
		}							
	}
	
	
	public static Double estimateX(Line2D seg, double yref) {
		double y = seg.getY2() - seg.getY1();
		
		if(Math.abs(y) < C.ERROR_RATE) //horizontal line
			return null;
		

		return seg.getX1() + (seg.getX2() - seg.getX1()) * (yref - seg.getY1()) / y;
	}
	
	/**
	 * using external library to find an intersection for points that are guaranteed to have one
	 * @param a segment 1
	 * @param b segment 2
	 * @return intersection point
	 */
	public static Point2D computeIntersectionLine2D(Line2D a, Line2D b) {
		double[] intersection = new double[2];
		
		Geometry.findLineSegmentIntersection(a.getX1(), a.getY1(), a.getX2(), a.getY2(),
											 b.getX1(), b.getY1(), b.getX2(), b.getY2(),
											 intersection);
		
		return new Point2D.Double(intersection[0], intersection[1]);
	}
	
	/**
	 * last encapsulating visual help for double comparison
	 * @return
	 */
	public static boolean dE(double a, double b) {
		return (Math.abs(a-b) < C.ERROR_RATE);
	}
	
	
}
