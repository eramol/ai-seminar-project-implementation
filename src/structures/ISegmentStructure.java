package structures;

import java.util.List;

/**
 * Interface for the segment structure, which is used to efficiently detect neighbors
 * and supports operations upon the set of segments we use 
 * @author Martin
 *
 */
public interface ISegmentStructure {
	
	/**
	 * an operation that handles an event point
	 * guarantees the segment structure consistency after dealing with the segments
	 * 
	 * @param e event point to be handled
	 * @return maximally 2 new events/intersection points
	 *
	 */
	public List<Event> handleEventPoint(Event e);
		
}
