package structures;

import java.awt.geom.Point2D;


/**
 * Interface that supports effective operations upont the event queue
 * @author Martin
 *
 */
public interface IEventQueue {

	/**
	 * searches for an event corresponding to a point p
	 * @param p
	 * @return null or found Event
	 */
	public Event search(Point2D p);
	
	
	/**
	 * searches for an event e (necessary encapsulation, used by the search(Point2D))
	 * @param p
	 * @return null or found Event
	 */
	public Event search(Event e);
	
	/**
	 * adds an event to the eventQueue
	 * @param e event to be added
	 */
	public void add(Event e);
	
	/**
	 * removes an event that is to be processed in the sweepline
	 * @return the event of the sweepline
	 */
	public Event removeHead();
		
}
