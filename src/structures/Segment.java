package structures;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

/**
 * Class representing a segment
 * 
 * Please note that the comparison requires a Y reference correctly set in StaticSweepReference
 * otherwise two segments are incomparable
 * 
 * note that the 'equals' method has not been overwritten so the segments
 * are compared for equality based on their position in memory
 * 
 * @author Martin
 *
 */
public class Segment implements Comparable<Segment>{	
	public final Line2D seg;
	public final int label;
	
	public Segment(Line2D seg, int label) {
		this.seg = seg;
		this.label = label;
	}

	/**
	 * not very clean	 
	 */
	@Override
	public int compareTo(Segment o) {
		return C.compareLineSubjectToStaticY(seg, o.seg);
//		int tmp =  C.compareLineSubjectToStaticY(seg, o.seg);
//		System.out.println(tmp + " " + seg.getP1() + " " + o.seg.getP1());
//		return tmp;
	}
	
	

	
}
