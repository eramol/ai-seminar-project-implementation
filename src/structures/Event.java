package structures;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 * represents an event point
 * in the event point there can be a start of a segment, end and an intersection (crossing)
 * @author Martin
 *
 */
public class Event implements Comparable<Event> {
	
	
	//upper, crossing and lower points
	private List<Segment> u, c, l;
	private Point2D point;
	
	
	
	public Event(Point2D point) {
		this(point, new ArrayList<>(),new ArrayList<>(),new ArrayList<>());		
	}

	public Event (Point2D point, List<Segment> u, List<Segment> c, List<Segment> l){
		this.point = point;
		this.u = u;
		this.c= c;
		this.l = l;
	}
	
	public Point2D getPoint() {
		return point;
	}

	/**
	 * @return segments which start at the event point
	 */
	public List<Segment> getU() {
		return u;
	}

	/**
	 * @return segments which cross the event point
	 */
	public List<Segment> getC() {
		return c;
	}

	/**
	 * @return segments which end at the event point
	 */
	public List<Segment> getL() {
		return l;
	}

	@Override
	public int compareTo(Event o) {
		// - => < ; + => >; 0 => == 
		return C.comparePoints(point, o.point);
	}

	
	@Override
	public int hashCode() {
		//needed for hash map
		return (int) (Math.floor(this.point.getX()) + Math.floor(this.point.getY())); 		
	}

	@Override
	public boolean equals(Object obj) {
		return C.comparePoints(this.point, ((Event) obj).point) == 0;
	}	
	
	
	
}
