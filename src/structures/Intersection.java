package structures;

import java.awt.geom.Point2D;

// Represents an intersection between two segments
public class Intersection {
	
	private Segment seg1;
	private Segment seg2;
	private Point2D intersection;
	
	public Intersection(Segment seg1, Segment seg2, Point2D intersection){
		this.setSeg1(seg1);
		this.setSeg2(seg2);
		this.setIntersection(intersection);
	}

	/**
	 * @return the intersection
	 */
	public Point2D getIntersection() {
		return intersection;
	}

	/**
	 * @param intersection the intersection to set
	 */
	public void setIntersection(Point2D intersection) {
		this.intersection = intersection;
	}

	/**
	 * @return the seg1
	 */
	public Segment getSeg1() {
		return seg1;
	}

	/**
	 * @param seg1 the seg1 to set
	 */
	public void setSeg1(Segment seg1) {
		this.seg1 = seg1;
	}

	/**
	 * @return the seg2
	 */
	public Segment getSeg2() {
		return seg2;
	}

	/**
	 * @param seg2 the seg2 to set
	 */
	public void setSeg2(Segment seg2) {
		this.seg2 = seg2;
	}

}
