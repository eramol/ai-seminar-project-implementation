package structures.imple;

import java.awt.geom.Point2D;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import structures.Event;
import structures.IEventQueue;

/**
 * an implementation of an event queue using a map and a priority queue
 * @author Martin
 *
 */
public class MapPQEventQueue implements IEventQueue {
	PriorityQueue<Event> pq = new PriorityQueue<>();		
	Map<Event, Event> map2 = new HashMap<>();
	
	
	
	@Override
	public Event search(Point2D p) {
		return search(new Event(p));				
	}

	@Override
	public void add(Event e) {
		if(map2.containsKey(e))
			return;
		
		map2.put(e, e);
		pq.add(e);
	}

	@Override
	public Event removeHead() {		
		Event e = pq.poll();
		if(e == null)
			return null;		
		
		map2.remove(e);				
		return e;
	}

	@Override
	public Event search(Event e) {
		return map2.get(e);
	}

}
