package structures.imple;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import structures.C;
import structures.Event;
import structures.ISegmentStructure;
import structures.Segment;
import structures.StaticSweepReference;

/**
 * implementation of the segment structure by a sorted list
 * 
 * @author Martin
 *
 */
public class ListSegmentStructure implements ISegmentStructure {

	List<Segment> segments = new ArrayList<>();
	
	
	/**
	 * main implementation of the event handling
	 */
	@Override
	public List<Event> handleEventPoint(Event e) {
		//(!) reference point reverting
		StaticSweepReference.setRefPoint(e.getPoint());
		StaticSweepReference.restoreRefPoint();
		
//		System.out.println("\n" + e.getPoint());
		
		
		List<Segment> toInsert = new ArrayList<>();
		List<Segment> toDelete = new ArrayList<>();
		List<Segment> rlCandidates = new ArrayList<>();
		List<Event> newEvs = new ArrayList<>();
		
		Segment leftmost = null, rightmost = null, leftInner = null, rightInner = null;
		
		toDelete.addAll(e.getL());
		toDelete.addAll(e.getC());
		toDelete = new ArrayList<>(new LinkedHashSet<>(toDelete));
		Collections.sort(toDelete);
		
		
		/*
		 * the deletion must be done before the reference point is moved,
		 *  as the order or the segments change on the reference point
		 */
		int idx = deleteAllFromSegments(toDelete); 

		////
		//(!) reference point update
		////
		StaticSweepReference.setRefPoint(e.getPoint());
		
		
		
		toInsert.addAll(e.getU());
		toInsert.addAll(e.getC());
		toInsert.removeAll(e.getL()); //if the segment is finishing in there, remove it from the insertion		
		toInsert = new ArrayList<>(new LinkedHashSet<>(toInsert)); //remove duplicates		
		Collections.sort(toInsert);
		
		
		//preparation for intersection checking
		if(!toInsert.isEmpty()) {
			leftInner = toInsert.get(0);
			rightInner = toInsert.get(toInsert.size() - 1);
		}									
										
		if(idx == -1){
			idx = Collections.binarySearch(segments, toInsert.get(0));
			idx = -(idx + 1);
		}
		if(idx > segments.size())
			idx = segments.size() - 1;
		
		if(idx != 0)
			leftmost = segments.get(idx-1);
		
		if(idx != segments.size())
			rightmost = segments.get(idx);		
			
		//add all to be added
		for(Segment s : toInsert){
//			System.out.println("I: " + s);
			segments.add(idx++, s);
		}
		
		//and compute potential new intersections
		if(leftmost != null && leftInner != null)
			checkIntersectionAndAddEvent(leftmost, leftInner, newEvs);
					
		if(rightmost != null && rightInner != null)
			checkIntersectionAndAddEvent(rightmost, rightInner, newEvs);
		
		if(leftmost != null && rightmost != null && leftInner == null)
			checkIntersectionAndAddEvent(leftmost, rightmost, newEvs);
		
		
		//this is a repairing part which shouldn't be in the algo
		//slows down everything a lot but roughly corrects the intersection counts
		if(!toInsert.isEmpty() && !segments.isEmpty())
			for(Segment s : toInsert)
				for(Segment a : segments)
					checkIntersectionAndAddEvent(a, s, newEvs);
		
		return newEvs;
	}
	
	/**
	 * deletes a list of segments that should be deleted
	 * originally the list was sorted
	 * @param toDelete
	 * @return should return an index of the deletion
	 */
	public int deleteAllFromSegments(List<Segment> toDelete) {
		int idx = -1;		
		
		if(!toDelete.isEmpty()) {
			idx = segments.indexOf(toDelete.get(0));
			
			for(Segment s : toDelete){
				//simplification of the process below, problem with segment comparison
				idx = Math.min(idx, segments.indexOf(s));
				segments.remove(s);
			}
//				if(!segments.remove(s))
//					System.out.println("ND: " + s);
//				else
//					System.out.println("D: " + s);
//			}
		}
		
		
		
//		if(!toDelete.isEmpty()) {						 
//			idx = Collections.binarySearch(segments, toDelete.get(0));			
//			//we don't have to check since it's sorted						
//			for(int i = 0; i < toDelete.size(); i++) {
//				segments.remove(idx);
//			}
//		}
		return idx;
	}
	
	/**
	 * checks whether there is an intersection between the two segments
	 * if there is, it adds it to the new event list
	 * @param a segment 1
	 * @param b segment 2
	 * @param newEvs a list of new events that will be returned
	 */
	public void checkIntersectionAndAddEvent(Segment a, Segment b, List<Event> newEvs) {
		Event tmpe;
		if(a.seg.intersectsLine(b.seg)){
			tmpe = new Event(C.computeIntersectionLine2D(a.seg, b.seg));
			tmpe.getC().add(a);
			tmpe.getC().add(b);
			
			if(C.comparePoints(StaticSweepReference.getRefPoint(), tmpe.getPoint()) < 0) {
//				System.out.println(a.seg.getP1() + " " + b.seg.getP1() + " at " + tmpe.getPoint());
				newEvs.add(tmpe);
			}			
		}
	}
}
