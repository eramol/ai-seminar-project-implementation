package data;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * a class that will handle the data
 * is important for file reading and data dumping 
 * 
 * @author Martin
 *
 */
public class DataHandler {
	List<Line2D> segments = new ArrayList<>();
	
	/**
	 * auto-initialising file reader class
	 * @param name
	 */
	public DataHandler(String name) {
		readFile(name);
	}

	/**
	 * reads the input file and processes the raw data
	 * @param filename input file
	 */
	public void readFile(String filename){
		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line;
			Scanner scanner;
			while((line = br.readLine()) != null) {
				scanner = new Scanner(line);
				scanner.useLocale(Locale.ENGLISH);
				Double xstart, ystart, xend, yend;
				xstart = scanner.nextDouble();
				ystart = scanner.nextDouble();
				xend = scanner.nextDouble();
				yend = scanner.nextDouble();
				
				
				//ensuring of the right ordering
				if(yend > ystart || yend.equals(ystart) && xend < xstart) {
					double tmp = ystart;
					ystart = yend;
					yend = tmp;
					
					tmp = xstart;
					xstart = xend;
					xend = tmp;					
				}
				
				segments.add(new Line2D.Double(xstart, ystart, xend, yend));
			}
			br.close();
						
		} catch (FileNotFoundException e) {			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	};
	
	/**
	 * return results of the file reading
	 * those are represented by bounded 'lines', we use a class Line2D
	 * there is a small preprocessing - the
	 * @return
	 */
	public List<Line2D> getRawSegments() {		
		return segments;
	}
	
	/**
	 * Dumps the result into a file 
	 * @param l the result set
	 * @param filename target file 
	 * @throws UnsupportedEncodingException 
	 * @throws FileNotFoundException 
	 */
	public static void dumpResults(List<Point2D> l, String filename) throws FileNotFoundException, UnsupportedEncodingException {
		
		PrintWriter writer = new PrintWriter(filename, "UTF-8");
		for(Point2D p : l){
			if(p.getX() == -1.0 && p.getY() == -1.0) {
				writer.println("overlapping between two segments");
			} else {
				writer.println(p.getX() + " " + p.getY());
			}
		}
		writer.close();
	}

}
