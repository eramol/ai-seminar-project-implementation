package algo;

import java.awt.geom.Point2D;
import java.util.List;

/**
 * general interface for the segment intersection algorithm
 * @author Martin
 *
 */
public interface ISegmentIntersectionAlgo {
	
	/**
	 * sets up the basic structures needed for algo to run
	 * @param filename input file
	 * @param secondFilename second input file
	 */
	public void setup(String filename, String secondFilename);
	
	/**
	 * computation of the intersections
	 */
	public void run();
	
	/**
	 * returns the found intersections
	 * @return a list of intersections
	 */
	public List<Point2D> getResults(); 
}
