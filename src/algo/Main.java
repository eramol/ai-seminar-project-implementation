package algo;

import utils.Utility;

import java.awt.geom.Point2D;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import data.DataHandler;

public class Main {

	private static Options options = new Options();
	private static String[] arguments;
	final static Logger log = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) throws FileNotFoundException,
			UnsupportedEncodingException {
		
		// Add the help option
		options.addOption("h", "help", false, "show this help");
		
		// Add the basic algorithm option
		OptionBuilder.withArgName("input1 input2 output");
		OptionBuilder.hasArgs(3);
		OptionBuilder.withDescription("basic line segment intersections");
		Option basic = OptionBuilder.create("b");

		options.addOption(basic);
		
		// Add the sweep line algorithm option
		OptionBuilder.withArgName("input1 input2 output");
		OptionBuilder.hasArgs(3);
		OptionBuilder.withDescription("sweep line segment intersections");
		Option sweep = OptionBuilder.create("s");

		options.addOption(sweep);
		
		// Add the generate option
		OptionBuilder.withArgName("numberSegments output");
		OptionBuilder.hasArgs(2);
		OptionBuilder
				.withDescription("generate n random segments to the specified output file");
		Option generate = OptionBuilder.create("g");

		options.addOption(generate);
		
		// Parse the options given as parameters
		CommandLineParser parser = new BasicParser();

		try {
			CommandLine cmd = parser.parse(options, args);
			
			// Option help used
			if (cmd.hasOption("h"))
				help();
			
			// Option basic algorithm used
			if (cmd.hasOption("b")) {
				arguments = cmd.getOptionValues("b");
				String input1 = arguments[0];
				String input2 = arguments[1];
				String output = arguments[2];

				NaiveIntersectionAlgo naiveAlgo = new NaiveIntersectionAlgo();
				naiveAlgo.setup(input1, input2);
				naiveAlgo.run();
				List<Point2D> result = naiveAlgo.getResults();
				DataHandler.dumpResults(result, output);
				System.out.println("basic line algo is done");
			}
			
			// Option sweep algorithm used
			else if (cmd.hasOption("s")) {
				arguments = cmd.getOptionValues("s");
				String input1 = arguments[0];
				String input2 = arguments[1];
				String output = arguments[2];

				GeneralSweepIntersectionAlgo sweepAlgo = new GeneralSweepIntersectionAlgo(
						null, null);
				sweepAlgo.setup(input1, input2);
				sweepAlgo.run();
				List<Point2D> result = sweepAlgo.getResults();
				DataHandler.dumpResults(result, output);
				System.out.println("sweep line algo is done");
			}
			
			// Option generate used
			else if (cmd.hasOption("g")) {
				arguments = cmd.getOptionValues("g");
				int num = Integer.parseInt(arguments[0]);
				String output = arguments[1];

				Utility.Generate(num, output);
				System.out.println("generate is done");
			}

			else {
				log.log(Level.SEVERE, "Missing option");
				help();
			}

		} catch (ParseException e) {
			System.out.println("No option specified");
			help();
		}

		catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Too many arguments");
			help();
		}

	}

	private static void help() {

		// This prints out some help

		HelpFormatter formater = new HelpFormatter();
		formater.printHelp("Main", options);
		System.exit(0);

	}

}
