package algo;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import data.DataHandler;

public class NaiveIntersectionAlgo implements ISegmentIntersectionAlgo {
	
	List<Line2D> segments1;
	List<Line2D> segments2;
	ArrayList<Point2D> res;

	@Override
	public void setup(String filename, String secondFilename) {

		// Process the first input file
		segments1 = new DataHandler(filename).getRawSegments();
		
		// Process the second input file
		segments2 = new DataHandler(secondFilename).getRawSegments();
	}


	@Override
	public void run() {

		res = new ArrayList<Point2D>();
		
		// Test for intersections between any pairs of segments
		for(Line2D seg1 : segments1){
			for(Line2D seg2 : segments2){
				Point2D point = getIntersection(seg1, seg2);

				if (point != null) {
					res.add(point);
				}
			}
		}
	}
	
	// Return a list containing all the intersections found
	@Override
	public List<Point2D> getResults() {
		
		List<Point2D> lst = new ArrayList<>();
		for(Point2D p : res) {
			if (!lst.contains(p)) {
				lst.add(p);
			  }
		}
		return lst;
	}

	public Point2D getIntersection(Line2D segment1, Line2D segment2) {

		/**
		 * 
		 * Implementation of the algorithm described at
		 * http://stackoverflow.com/
		 * questions/563198/how-do-you-detect-where-two-line-segments-intersect
		 * 
		 * t = (q - p) x s / (r x s) u = (q - p) x r / (r x s)
		 * 
		 * numerator = (q - p) x r denominator = r x s
		 * 
		 */
		// Informations relative to segment1
		// Segment1 run from p to p+r
		Point2D p = segment1.getP1();
		Point2D r = substractPoints(segment1.getP2(), p);

		// Informations relative to segment2
		// Segment2 run from q to q+s
		Point2D q = segment2.getP1();
		Point2D s = substractPoints(segment2.getP2(), q);

		// Informations relative to the numerator
		// and the denominator
		double numerator = crossProduct(substractPoints(q, p), r);
		double denominator = crossProduct(r, s);

		// Informations relative to t and u

		double t = (crossProduct(substractPoints(q, p), s)) / denominator;
		double u = numerator / denominator;

		if (numerator == 0 && denominator == 0) {
			// The two lines are overlapping
			// We use the special Point2D(-1.0, -1.0) to indicate such
			// overlapping
			if ((dotProduct(substractPoints(q, p), r) >= 0 && dotProduct(
					substractPoints(q, p), r) <= dotProduct(r, r))
					|| (dotProduct(substractPoints(p, q), s) >= 0 && dotProduct(
							substractPoints(p, q), s) <= dotProduct(s, s))) {

				return new Point2D.Double(-1.0, -1.0);
			}
		}

		// The two lines are parallel and not intersecting
		if (numerator != 0 && denominator == 0) {
			return null;
		}
		
		// The two lines intersect at the point p + t*r
		if (denominator != 00 && t >= 0 && t <= 1 && u >= 0 && u <= 1) {

			double x = p.getX() + (t * r.getX());
			double y = p.getY() + (t * r.getY());

			Point2D res = new Point2D.Double(x, y);
			return res;
		}

		return null;

	}

	/**
	 * Calculate the cross product of the two points.
	 */
	public double crossProduct(Point2D point1, Point2D point2) {
		return (point1.getX() * point2.getY())
				- (point1.getY() * point2.getX());
	}

	/**
	 * Calculate the dot product
	 */
	public double dotProduct(Point2D point1, Point2D point2) {
		return (point1.getX() * point2.getX())
				+ (point1.getY() * point2.getY());
	}

	/**
	 * Subtract point 2 from point 1.
	 * 
	 */
	public Point2D substractPoints(Point2D point1, Point2D point2) {

		double x = point1.getX() - point2.getX();
		double y = point1.getY() - point2.getY();

		Point2D res = new Point2D.Double(x, y);

		return res;
	}

}
