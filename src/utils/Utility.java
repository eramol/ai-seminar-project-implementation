package utils;

import java.awt.geom.Point2D;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class Utility {
	
	// Class used to generate num_seg random segments to the specified output file
	public static void Generate(int num_seg, String outputFile)
			throws FileNotFoundException, UnsupportedEncodingException {

		Point2D up;
		Point2D low;
		int range = 100;

		PrintWriter writer = new PrintWriter(outputFile, "UTF-8");

		for (int i = 0; i < num_seg; i++) {

			int x1 = (int) (Math.random() * range);
			int x2 = (int) (Math.random() * range);
			int y1 = (int) (Math.random() * range);
			int y2 = (int) (Math.random() * range);

			if (y1 > y2) {
				up = new Point2D.Double(x1, y1);
				low = new Point2D.Double(x2, y2);
			} else {
				up = new Point2D.Double(x2, y2);
				low = new Point2D.Double(x1, y1);
			}

			writer.println(low.getX() + " " + low.getY() + " " + up.getX()
					+ " " + up.getY());
		}

		writer.close();

	}
}
